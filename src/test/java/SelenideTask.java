import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition.*;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.testng.ScreenShooter;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.url;
import static org.testng.AssertJUnit.assertEquals;

public class SelenideTask {

    @Test(priority = 0, alwaysRun = true)
    public void enterAppleSiteTest(){
        open("https://www.apple.com/");
        assertEquals(url(),"https://www.apple.com/");

        $$("[class*=\"ac-gn-link ac-gn-link\" ]").shouldHave(CollectionCondition.size(12));
    }

    @Test(priority = 1, enabled = false)
    public void enterTradeInPageTest(){
        open("https://www.apple.com/");
        $("[aria-label*=\"Learn more about trade in\"]").should(exist);
        $("[aria-label*=\"Learn more about trade in\"]").click();
        assertEquals(url(),"https://www.apple.com/shop/trade-in");
    }

    @Test(priority = 2, description = "trying to enter the iPhone XR buying page")
    public void IPhoneXRTest(){
        //isHeadless();
        open("https://www.apple.com/");
        $("[aria-label*=\"Buy now with trade in\"]").shouldBe(visible);
        $("[aria-label*=\"Buy now with trade in\"]").click();
        $("#noTradeIn").doubleClick();
        assertEquals(url(),"https://www.apple.com/shop/buy-iphone/iphone-xr");
    }

    @Test(priority = 3, successPercentage = 100)
    public void orderIPhoneXRTest(){
        ScreenShooter.captureSuccessfulTests = true;
        open("https://www.apple.com/shop/buy-iphone/iphone-xr");

        $("#noTradeIn").isDisplayed();
        $("#noTradeIn").hover();
        $("#noTradeIn").doubleClick();

        $("#dimensionColor-coral").exists();
        $("#dimensionColor-coral").contextClick().pressEscape();
        $("#dimensionColor-coral").doubleClick();

        $("#Item2256gb_label").shouldBe(visible);
        $("#Item2256gb_label").hover();
        $("#Item2256gb_label").click();

        $("#Item33_label").shouldBe(appear);
        $("#Item33_label").hover().click();

        $("#Item40_label").is(appears);
        $("#Item40_label").shouldHave(attribute("id"));
        $("#Item40_label").hover().click(2,2);

        $("[class*=\"as-purchaseinfo-actioninfo\"]").shouldBe(visible).shouldHave(text("Continue"));
        $("[class*=\"as-purchaseinfo-actioninfo\"]").hover().click();
    }

    @Test(priority = 4, invocationCount = 3)
    public void addToBagTest(){
        //isHeadless();
        open("https://www.apple.com/shop/buy-iphone/iphone-xr?cppart=UNLOCKED/US&product=MT0K2LL/A&purchaseOption=fullPrice&step=warranty#");
        $("[class*=\"as-summaryheader-producttitle\"]").shouldHave(text("iPhone XR 256GB Coral"));
        $("[class*=\"as-summaryheader-productprice\"]").shouldHave(text("$899.00\n"));

        $("[class*=\"merchandising button\"]").hover().click();

        $("[title*=\"Review Bag\"]").hover().click();
        assertEquals(url(),"https://www.apple.com/shop/bag");
    }
}
